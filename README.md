# Build image for kernal development Aarch64 linaro

## purpose

This project creates a CentOS docker Image to build mailine kernel, uboot, with the `odagrun`.

Uses [gioxa/imagebuilder-c7](https://microbadger.com/images/gioxa/imagebuilder-c7)] and builds with `odagrun` on `openshift online starter` as **non-root** and **non-privileged**.

## installed Packages

- core-utils

### Group install:

- @Development Tools

### manual packages

- [gcc-arm-8.2-2019.01-x86_64-aarch64_be-linux-gnu.tar.xz](https://armkeil.blob.core.windows.net/developer/Files/downloads/gnu-a/8.2-2019.01/gcc-arm-8.2-2019.01-x86_64-aarch64_be-linux-gnu.tar.xz)
